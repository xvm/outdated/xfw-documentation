Структура репозитория
=====================

* **build**: Различные утилиты, необходимые для сборки, такие как FlashDevelop build и RABCDAsm;
* **src**
   * **actionscript**: Actionscript3 часть XFW
   * **python**: Python часть XFW
   * **swf**: патчи для SWF файлов
* **build.sh**: Скрипт для сборки XFW

Actionscript
------------

Содержит декомпилированный код lobby.swf, а также код Actionscript компонента XVM Framework.

* ``/src/com/xfw/``:
   * ``infrastructure/`` :
   * ``WGUtils.as`` : Вспомогательные функции для работы с игрой и ее данными. Например, выделение тега клана из имени пользователя или показ всплывающей подсказки.
   * ``XfwView.as``
* ``/src/com/xfw_shared/``:
   * ``events/`` :
   * ``GraphicsUtil.as`` : работа с RGB-данными (осветление, затемнение, конвертация 3хInt <-> 0xXXXXXX )
   * ``JSONx(Error,Loader).as`` : работа с JSONx (JSON, в котором разрешены комментарии)
   * ``Logger.as`` : ведение журнала
   * ``ObjectConverter.as`` :
   * ``PhpDate.as`` : ActionScript3 версия PhpDate(). Предназначено для генерации строки с текущей датой по шаблону.
   * ``Sprintf.as`` : ActionScript3 версия printf(). Предназначено для генерации произвольной строки по шаблону.
   * ``SysUtils.as`` : Вспомогательные функции, такие как ожидание события.
   * ``Xfw(Const,Links,Utils)``
* ``/wg/`` : декомпилированные файлы от Wargaming
* ``/xfwfonts/`` : Файлы шрифтов XFW
* ``/.build-swc-(wg,xfw).sh`` : скрипты для сборки wg.swc и xfw.swc. Не предназначены для непосредственного запуска
* ``/build.sh`` : скрипт сборки
* ``/wg.as3project`` : Проект FlashDevelop для сборки wg.swc
* ``/xfw.as3project`` : Проект FlashDevelop для сборки xfw.swc и xfw.swf
* ``/xfwfonts.as3project`` : Проект FlashDevelop для сборки xfwfonts.swf

Python
------

Python часть XVM Framework расположена в каталоге ``/src/python/``.

Она состоит из

* ``/build.sh`` : скрипт сборки
* ``/scripts/client/gui/mods/mod_.py`` : Точка входа для XVM Framework
* ``/mods/xfw/python/``: Каталог с исходным кодом Python части
   * ``lib/``: Сторонние библиотеки
   * ``xfw/`` : Основные файлы Python-части XVM Framework
      * ``__init__.py`` : Инициализация
      * ``constants.py`` : Константы
      * ``events.py`` : Работа с событиями
      * ``logger.py`` : Логгер
      * ``singleton.py`` : Синглтон
      * ``swf.py`` : Загрузка ``xfw.swf`` и ``xfwfonts.swf``
      * ``utils.py`` : Вспомогательные функции общего назначения
      * ``wg.py`` : Вспомогательные функции, связанные с данными WoT
      * ``xfwmodinfo.py`` : Вспомогательный класс для учета модификаций
      * ``xfwview.py`` : Работа с ActionScript3 модификациями
   * ``xfw_empty.py`` : Файл-пустышка. Используется загрузчиком XVM Framework.
   * ``xfw_loader.py`` : Загрузчик XVM Framework

SWF
---

Содержит патчи для SWF файлов.

Основная цель патчей - повышение области видимости для переменных и функций, которые находятся в lobby.swf

* ``/flash/``: оригинальные файл от Wargaming.
* ``/build.sh``: скрипт сборки
* ``/*.patch`` : патчи