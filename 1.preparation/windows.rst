Windows
=======


Зависимости
-----------

* Которые необходимо установить самостоятельно
   * Python 2.7. http://python.org/
   * Java. http://java.com/en/download/manual.jsp
   * MSYS2. https://msys2.github.io/
   * Apache Flex SDK. http://flex.apache.org/

* Уже содержаться в репозитории
   * RABCDAsm. https://github.com/CyberShadow/RABCDAsm/releases

* Желательно установить для комфортной работы
   * FlashDevelop. http://flashdevelop.org/

Установка MSYS2
---------------

1. Скачиваем с https://msys2.github.io/ установщик. Предпочтительно использовать 64-битную версию.
2. Устанавливаем,
3. Добавляем ``<MSYS2_DIR>/usr/bin`` в **начало** переменной окружения ``PATH``.
4. В ``<MSYS2_DIR>/msys2.ini`` устанавливаем ``MSYS2_PATH_TYPE=inherit``
5. В ``<MSYS2_DIR>/msys2_shell.cmd`` удаляем ``rem`` перед ``set MSYS2_PATH_TYPE=inherit``
6. Открываем ``<MSYS2_DIR>/msys2.exe`` и выполняем команды:

::

  pacman -S patch coreutils zip unzip wget


Установка Apache Flex SDK
-------------------------

1. Идем на http://flex.apache.org/download-binaries.html и скачиваем ``apache-flex-sdk-4.15.0-bin.zip``
2. Распаковываем архив в ``<FLEX_DIR>`` и добавляем переменные окружения:

   1. Создаем ``FLEX_HOME`` со значением ``<FLEX_DIR>``
   2. Добавляем ``%FLEX_HOME%/bin`` в ``PATH``

3. Закачиваем https://github.com/nexussays/playerglobal/archive/master.zip и распаковываем содержимое каталога ``playerglobal-master`` в ``%FLEX_HOME%/frameworks/libs/player/``

Установка Python
----------------

1. Скачиваем Python 2.7 x86 с https://www.python.org/
2. Добавляем ``<PYTHON_DIR>`` в ``PATH``, если инсталлятор не сделал этого сам.
3. Переходим в ``<PYTHON_DIR>`` и создаем копию ``python.exe`` под именем ``python2.7.exe``

