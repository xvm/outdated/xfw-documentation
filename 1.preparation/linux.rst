Linux
=====

Зависимости
-----------

- Bash http://www.gnu.org/software/bash/
- Coreutils http://www.gnu.org/software/coreutils/
- Diffutils http://www.gnu.org/software/diffutils/
- Patch http://www.gnu.org/software/patch/

- Git http://git-scm.com/
- Mercurial http://mercurial.selenic.com/

- Mono http://www.mono-project.com/
- Python 2.7 http://www.python.org/
- OpenJRE 6/7/8 http://www.oracle.com/technetwork/java/javase/downloads/index.html

- Zip http://www.info-zip.org/Zip.html

Установите эти программы используя ваш менеджер пакетов.

Для дистрибутивов, основанных на .deb пакетах, команда будет выглядеть примерно так:

::

   sudo apt-get install bash coreutils diffutils patch git mercurial mono-devel python2.7 default-jre zip


Установка Apache Flex
---------------------

1. Скачайте архив с Apache Flex c официального сайта ( http://flex.apache.org/download-binaries.html )
2. Распакуйте архив в  ```/opt/apache-flex/```
3. Установите ``playerglobal.swf`` используя команду

::

   git clone git://github.com/nexussays/playerglobal.git frameworks/libs/player


Замечание
---------

Для сборки XVM Framework вам будет необходима программа **RABCDAsm**. Версия для архитектуры amd64 уже находится в репозитории с XVM Framework.

Однако, если вы захотите выполнить сборку XVM Framework на другой архитектуре, то вам будет необходимо самостоятельно собрать RABCDAsm под нужную вам архитектуру, и разместить бинарные файлы программы в каталоге ``<корень репозитория>/build/bin/Linux_<aрхитектура>/``.

Исходный код RABCDAsm можно найти на `github.com <https://github.com/CyberShadow/RABCDAsm>`_

